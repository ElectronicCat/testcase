<?php

namespace App;

use Mookofe\Tail\Facades\Tail;
use App\Exceptions\ValidationException;

class Transporter implements TransporterInterface
{
    const QUEUE_PREFIX = 'test_case_queue_';

    /**
     * Valid offset fot message timestamp
     * (if Producer and Consumer are located in different time zones)
     */
    const TIMESTAMP_VALID_OFFSET = 0;

    /**
     * How many times one messages can throw processing Exceptions
     */
    const PROCESSING_ERRORS_LIMIT = 2;

    public function __construct(){}

    /**
     * We're reading messages with set version from Rabbit
     *
     * @param int $version
     * @param \Closure $callback
     * @return bool
     */
    public function read($version, \Closure $callback)
    {
        $queueName = $this->getQueueName($version);
        /*Here we're catching validation Exceptions only.
        If input data is correct, we should use another logic
        */
        try
        {
            Tail::listen($queueName, function ($message) use($version, $callback) {
                $data = json_decode($message, true);
                list($name, $value, $time) = $this->validate(
                    isset($data['name'])    ? $data['name']     : false,
                    isset($data['value'])   ? $data['value']    : false,
                    isset($data['time'])    ? $data['time']     : false);
                /**
                 * @var int
                 */
                $processingErrorsCounter = isset($data['processingErrorsCounter']) ? (int) $data['processingErrorsCounter'] : 0;
                //Here we're catching processing errors. Database errors etc.
                try
                {
                    $callback($version, $name, $value, $time);
                }
                catch (\Exception $e)
                {
                    //Look for comments in my e-mail (1)
                    if($processingErrorsCounter < static::PROCESSING_ERRORS_LIMIT) {
                        $this->write($version, $name, $value, $time, $processingErrorsCounter + 1);
                    }
                }
            });
        }
        catch (ValidationException $e)
        {
            //This message is hopeless
            echo "Incorrect data: ". $e->getMessage() ."\n";
        }
        return true;
    }

    /**
     * Write data to Rabbit
     *
     * @param int $version
     * @param string $name
     * @param float $value
     * @param int $time
     * @param int $processingErrorsCounter
     * @return bool
     * @throws ValidationException
     */
    public function write($version, $name, $value, $time, $processingErrorsCounter = 0)
    {
        $queueName  = $this->getQueueName($version);
        list($name, $value, $time) = $this->validate($name, $value, $time);
        $message    = json_encode(compact('name', 'value', 'time', 'processingErrorsCounter'));
        Tail::add($queueName, $message);
        return true;
    }

    /**
     * Get queue name by prefix and version
     *
     * @param $version
     * @return string
     */
    protected function getQueueName($version)
    {
        return static::QUEUE_PREFIX . $version;
    }

    /**
     * Validation of our data
     *
     * @param $name
     * @param $value
     * @param $time
     * @return array
     * @throws ValidationException
     */
    protected function validate($name, $value, $time)
    {
        if( empty($name) || !is_string($name) ) {
            throw new ValidationException("Invalid 'name': ". $name);
        }
        if( (!is_numeric($value)) || ((double) $value) < 1 || ((double) $value > 2) ) {
            throw new ValidationException("Invalid 'value': ". $value);
        }
        if( (!is_numeric($time)) || ((int) $time) > time() + static::TIMESTAMP_VALID_OFFSET ) {
            throw new ValidationException("Invalid 'time': ". $time);
        }
        return [$name, (double) $value, (int) $time];
    }
}