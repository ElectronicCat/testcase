<?php

namespace App\Console\Commands;

use App\Exceptions\ValidationException;
use Illuminate\Console\Command;
use App\Transporter;
use App\Message;

class Consumer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consumer:read
                            {version : version of messages that process should list (from 1 to 10)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read a message from rabbit and save it to database';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws ValidationException
     */
    public function handle()
    {
        /**
         * @var int
         */
        $version = (int)  $this->argument('version');
        if($version < 1 || $version > 10) {
            throw new ValidationException("Version ". $version ." is invalid");
        }

        /*
         * Set version as argument for potential future logic with many versions
         */
        (new Transporter())->read($version, function($version, $name, $value, $time)
        {
            $message = new Message();
            $message->version   = $version;
            $message->name      = $name;
            $message->value     = $value;
            $message->time      = $time;
            $message->save();
        });
    }
}
