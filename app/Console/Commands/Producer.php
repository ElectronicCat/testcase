<?php

namespace App\Console\Commands;

use App\Message;
use App\Transporter;
use Illuminate\Console\Command;

class Producer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'producer:write';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a message and send it to a rabbit';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        while(true) {
            $version    = rand(1, 10);
            $nameValues = Message::getAvailableNames();
            $name       = $nameValues[array_rand($nameValues)];
            $value      = 1 + rand(1, 100000) / 100000;
            $timestamp  = time();
            (new Transporter())->write($version, $name, $value, $timestamp);
            //For manual testing
            sleep(1);
        }
    }
}
