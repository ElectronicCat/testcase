<?php

namespace App;

interface TransporterInterface
{
    /**
     * Get data from ESB
     *
     * @param int $version
     * @param \Closure $callback
     * @return bool
     */
   public function read($version, \Closure $callback);

    /**
     * Set specified data to ESB
     *
     * @param int $version
     * @param string $name
     * @param double $value
     * @param int $time
     * @param int $processingErrorsCounter
     * @return bool
     */
    public function write($version, $name, $value, $time, $processingErrorsCounter = 0);

}