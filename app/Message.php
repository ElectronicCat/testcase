<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * Valid values for field "name" in messages
     *
     * @var array
     */
    static protected $availableNames = [
        'EUR/USD',
        'USD/EUR',
        'JPY/GBP',
        'GBP/JPY',
        'EUR/JPY',
        'JPY/EUR',
        'USD/GBP',
        'GBP/USD'
    ];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages';

     /**
     * We aren't using 'updated_at'
     *
     * @var boolean
     */
    public $timestamps  = false;

    /**
     * Just a getter for self property
     *
     * @return array
     */
    static public function getAvailableNames()
    {
        return self::$availableNames;
    }
}
