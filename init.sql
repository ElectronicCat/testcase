CREATE TABLE public.messages
(
  id bigint NOT NULL DEFAULT nextval('messages_id_seq'::regclass),
  version integer,
  name character varying(31),
  value double precision,
  "time" integer,
  CONSTRAINT messages_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);